<?php 

class ModelRGenRGen extends Model {

	public $tbl_theme 			= 'rgen_theme';
	public $tbl_theme_storage	= 'rgen_theme_storage';
	public $tbl_module 			= 'rgen_modules';
	public $tbl_module_customize = 'rgen_modules_customize';
	public $tbl_settings 		= 'rgen_settings';

	public function getTheme($group, $store_id = 0) {
		$data = array();
		$query = $this->db->query(
			"SELECT * FROM " . DB_PREFIX . $this->tbl_theme . " WHERE 
			`store_id`	= '" . (int)$store_id . "' AND 
			`group`		= '" . $this->db->escape($group) . "'
		");
		foreach ($query->rows as $key => $value) {
			$data[$value['section']][$value['key']] = json_decode($value['value']);
		}
		return $this->rgen->factory->objToArray($data);
	}

	public function getModtheme($group, $store_id = 0) {
		$data = array();
		$query = $this->db->query(
			"SELECT * FROM " . DB_PREFIX . $this->tbl_module_customize . " WHERE 
			`store_id`	= '" . (int)$store_id . "' AND 
			`group`		= '" . $this->db->escape($group) . "'
		");
		foreach ($query->rows as $key => $value) {
			$data[$value['section']][$value['key']] = json_decode($value['value']);
		}
		return $this->rgen->factory->objToArray($data);
	}

	public function getModules($group, $store_id = 0) {
		$data = array();
		$query = $this->db->query(
			"SELECT * FROM " . DB_PREFIX . $this->tbl_module . " WHERE 
			`store_id`	= '" . (int)$store_id . "' AND 
			`group`		= '" . $this->db->escape($group) . "'
		");
		foreach ($query->rows as $key => $value) {
			$data[$value['section']][$value['key']] = json_decode($value['value']);
		}
		return $this->rgen->factory->objToArray($data);
	}

	public function getSettings($group, $store_id = 0) {
		$data = array();
		$query = $this->db->query(
			"SELECT * FROM " . DB_PREFIX . $this->tbl_settings . " WHERE 
			`store_id`	= '" . (int)$store_id . "' AND 
			`group`		= '" . $this->db->escape($group) . "'
		");
		foreach ($query->rows as $key => $value) {
			$data[$value['section']][$value['key']] = json_decode($value['value']);
		}
		return $this->rgen->factory->objToArray($data);
	}

}
?>