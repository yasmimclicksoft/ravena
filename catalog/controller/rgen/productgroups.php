<?php
class ControllerRgenProductgroups extends Controller {
	public function index($setting) {
		$data = array();
		$factory = $this->rgen->factory;
		
		/* Get module data
		------------------------*/
		if ($factory->node($setting, 'module_type', 0,0) == 'gridmanager') {
			$part = explode('.', $factory->node($setting, 'setting_key', 0,0));
		}else{
			$part = explode('.', $setting);	
		}

		if (isset($part)) {
			if ($part[1] === 'gridmanager') {
				$module_settings      = $setting;
			}else{
				$module_settings      = $this->rgen->storage->get('modules_data', $part[0].'_set');
				$module_settings      = $module_settings[$part[1]]['data'][0];	
			}
			
			$module               = $this->rgen->storage->get('modules_data', $part[0]);
			$module_common        = $module[$part[2]]['common'];
			$module_data          = $module[$part[2]]['data'];
			$module_key           = 'productgroups';
			
			$data['lng']          = $this->config->get('config_language_id');
			$data['module_name']  = 'rgen-'.$module_key;
			$data['module_id']    = $part[1];
			$data['module_class'] = ' '.$module_key.'-'.$part[2];
			$data['settings']     = $module_settings;
			
			/* Construct module
			------------------------*/
			if (isset($module_data) && $this->chk($module_data)) {
				$data['modules'] = array();
				$data['modules']['config'] = $module_common;
				foreach ($module_data as $key => $value) {
					if ($value['status']) {
						$data['modules']['data'][] = array(
							'type'         => $value['node_type'],
							'title_status' => $value['item_data']['content']['title_status'],
							'title'        => $this->lngdata($value['item_data']['content']['title']),
							'prd_style'    => $value['item_data']['setting']['prd_style'],
							'prd_view'     => $value['item_data']['setting']['prd_view'],
							'prd_limit'    => $value['item_data']['setting']['prd_limit'],
							'grids'        => isset($value['item_data']['setting']['grids']['classGroup']) ? ' '.$value['item_data']['setting']['grids']['classGroup'] : ' eq4 d-eq4 t-eq4 mxl-eq2 msm-eq2 mxs-eq1 gt10 mb10',
							'carousel'     => $value['item_data']['setting']['carousel'],
							'cssclass'     => $value['item_data']['setting']['cssclass'],
							'products'     => $this->getProducts($value['node_type'], $value['item_data']['setting'])
						);
					}
				}
			}
		}

		/* Render
		------------------------*/
		if (file_exists(DIR_TEMPLATE . DIR_FRONT_MODULE . $module_key . '.tpl')) {
			return $this->load->view(DIR_FRONT_MODULE . $module_key . '.tpl', $data);
		}
	}

	/* Product functions
	------------------------*/
	private function getProducts($type, $arr) {
		$this->load->model('catalog/product');
		$tmp = array();

		switch ($type) {
			case 'special':
				$filter_data = array(
					'sort'  => 'pd.name',
					'order' => 'ASC',
					'start' => 0,
					'limit' => $arr['prd_limit']
				);
				$results = $this->model_catalog_product->getProductSpecials($filter_data);

				$img_w = isset($arr['image_w']) ? $arr['image_w'] : 250;
				$img_h = isset($arr['image_h']) ? $arr['image_h'] : 250;

				return $this->products($results, $img_w, $img_h);
				break;

			case 'latest':
				$filter_data = array(
					'sort'  => 'p.date_added',
					'order' => 'DESC',
					'start' => 0,
					'limit' => $arr['prd_limit']
				);
				$results = $this->model_catalog_product->getProducts($filter_data);
				
				$img_w = isset($arr['image_w']) ? $arr['image_w'] : 250;
				$img_h = isset($arr['image_h']) ? $arr['image_h'] : 250;

				return $this->products($results, $img_w, $img_h);
				break;

			case 'best':

				$results = $this->model_catalog_product->getBestSellerProducts($arr['prd_limit']);
				
				$img_w = isset($arr['image_w']) ? $arr['image_w'] : 250;
				$img_h = isset($arr['image_h']) ? $arr['image_h'] : 250;

				return $this->products($results, $img_w, $img_h);
				break;

			case 'category':
				if (isset($arr['category']['id'])) {
					$filter_data = array(
						'filter_category_id' => $arr['category']['id'],
						'filter_filter'      => '',
						'sort'               => 'p.sort_order',
						'order'              => 'ASC',
						'start'              => 0,
						'limit'              => $arr['prd_limit']
					);

					$results = $this->model_catalog_product->getProducts($filter_data);
					
					$img_w = isset($arr['image_w']) ? $arr['image_w'] : 250;
					$img_h = isset($arr['image_h']) ? $arr['image_h'] : 250;

					return $this->products($results, $img_w, $img_h);
				}
				break;

			case 'brand':
				if (isset($arr['brand']['id'])) {
					$filter_data = array(
						'filter_manufacturer_id' => $arr['brand']['id'],
						'sort'                   => 'p.sort_order',
						'order'                  => 'ASC',
						'start'                  => 0,
						'limit'                  => $arr['prd_limit']
					);

					$results = $this->model_catalog_product->getProducts($filter_data);
					
					$img_w = isset($arr['image_w']) ? $arr['image_w'] : 250;
					$img_h = isset($arr['image_h']) ? $arr['image_h'] : 250;

					return $this->products($results, $img_w, $img_h);
				}
				break;

			case 'custom':
				if (isset($arr['products']) && sizeof($arr['products']) > 0) {
					foreach ($arr['products'] as $key => $value) {
						$results[] = $this->model_catalog_product->getProduct($value['id']);
					}
					$img_w = isset($arr['image_w']) ? $arr['image_w'] : 250;
					$img_h = isset($arr['image_h']) ? $arr['image_h'] : 250;

					return $this->products($results, $img_w, $img_h);
				}
				break;
		}
	}

	private function products($results, $img_w, $img_h) {
		$tmp = array();
		if (isset($results) && sizeof($results) > 0) {
			foreach ($results as $result) {
				$image = $this->imgresize($result['image'], $img_w, $img_h);

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = $result['rating'];
				} else {
					$rating = false;
				}

				$tmp[] = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'name'        => $result['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'rating'      => $rating,
					'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
				);
			}
		}
		return $tmp;
	}

	/* Helper functions
	------------------------*/
	private function icon($arr) {
		$tmp = array();
		if (isset($arr['status']) && $arr['status']) {
			if ($arr['type'] == 'ico') {
				$tmp = array(
					'type'  => $arr['type'],
					'icon'  => $arr['icon'],
					'css'  => $arr['css']
				);
			} else {
				$tmp = array(
					'type'  => $arr['type'],
					'icon'  => $this->rgen->factory->imgpath($arr['image']),
					'css'  => $arr['css']
				);
			}
			return $tmp;
		} else {
			return false;
		}
	}
	private function img($val){
		return str_replace("../image/","image/",$val);
	}
	private function chk($val){
		return $this->rgen->factory->checkdata($val);
	}
	private function chkstr($val, $str){
		return $this->rgen->factory->checkstr($val, $str);
	}
	private function lngdata($val) {
		$lng = $this->rgen->storage->get('language', 'language');
		return $this->rgen->factory->lngdata($val, $lng);
	}
	private function imgresize($val, $w, $h) {
		$this->load->model('tool/image');
		return $this->rgen->factory->imgresize($val, $w, $h, $this->model_tool_image);
	}


}