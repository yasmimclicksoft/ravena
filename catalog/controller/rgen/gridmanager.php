<?php
class ControllerRgenGridmanager extends Controller {
	public function index($setting) {
		$data = array();

		/* Get module data
		------------------------*/
		$part = explode('.', $setting);

		if (isset($part)) {
			$module_settings      = $this->rgen->storage->get('modules_data', $part[0].'_set');
			$module_settings      = $module_settings[$part[1]]['data'][0];
			
			$module               = $this->rgen->storage->get('modules_data', $part[0]);
			$module_common        = $module[$part[2]]['common'];
			$module_data          = $module[$part[2]]['data'];
			$module_key           = 'gridmanager';
			
			$data['lng']          = $this->config->get('config_language_id');
			$data['module_name']  = 'rgen-'.$module_key;
			$data['module_id']    = $part[1];
			$data['module_class'] = ' '.$module_key.'-'.$part[2];
			$data['settings']     = $module_settings;
			
			/* Construct module
			------------------------*/
			if (isset($module_data) && $this->chk($module_data)) {
				$data['modules'] = array();
				$data['modules']['config'] = $module_common;
				foreach ($module_data as $key => $value) {
					if ($value['status']) {
						$data['modules']['data'][] = array(
							'node_type' => $value['node_type'],
							'row'       => $value['item_data'],
							'columns'   => $this->columnData($value['items'])
						);
					}
				}
			}
		}

		/* Render
		------------------------*/
		if (file_exists(DIR_TEMPLATE . DIR_FRONT_MODULE . $module_key . '.tpl')) {
			return $this->load->view(DIR_FRONT_MODULE . $module_key . '.tpl', $data);
		}
	}

	/* Block content functions
	------------------------*/
	private function columnData($arr){
		$tmp = array();
		if (isset($arr) && sizeof($arr) > 0 && is_array($arr)) {
			foreach ($arr as $key => $value) {
				if ($value['status']) {
					//echo "<pre>".print_r($value,true)."</pre>";
					$tmp[] = array(
						'node_type'     => $value['node_type'],
						'col_size'      => $this->rgen->factory->node($value, 'item_data/column/classGroup', 0,0),
						'col_class'     => $this->rgen->factory->node($value, 'item_data/customize/cssclass', 0,0) != '' ? ' '.$this->rgen->factory->node($value, 'item_data/customize/cssclass', 0,0) : null,
						'col_style'     => $this->rgen->factory->node($value, 'item_data/customize/block', 1),
						'col_bg'        => $this->img($this->rgen->factory->node($value, 'item_data/customize/bg_img', 1)),
						'content_items' => $this->itemsData($value['items'])
					);
				}		
			}
		}
		return $tmp;
	}
	private function itemsData($arr)
	{
		//$this->load->controller('module/revslideroutput', $rev_setting);
		$tmp = array();
		if (isset($arr) && sizeof($arr) > 0 && is_array($arr)) {
			foreach ($arr as $key => $value) {
				if ($value['status']) {
					$tmp[] = array(
						'node_type' => $this->rgen->factory->node($value, 'item_data/setting/block_type', 0,0),
						'cssclass' => $this->rgen->factory->node($value, 'item_data/setting/cssclass', 0,0),
						'html'      => $this->lngdata($this->rgen->factory->node($value, 'item_data/setting/html', 0,0)),
						'mod_data'  => $this->moduledata($this->rgen->factory->node($value, 'item_data/setting', 0,0))
					);
				}
			}
		}
		return $tmp;
	}


	/* Helper functions
	------------------------*/
	private function icon($arr) {
		$tmp = array();
		if (isset($arr['status']) && $arr['status']) {
			if ($arr['type'] == 'ico') {
				$tmp = array(
					'type'  => $arr['type'],
					'icon'  => $arr['icon'],
					'css'  => $arr['css']
				);
			} else {
				$tmp = array(
					'type'  => $arr['type'],
					'icon'  => $this->rgen->factory->imgpath($arr['image']),
					'css'  => $arr['css']
				);
			}
			return $tmp;
		} else {
			return false;
		}
	}
	private function moduledata($val) {
		$mod_name                   = $this->rgen->factory->node($val, 'mod_data/section', 0,0);
		$mod_id                     = $this->rgen->factory->node($val, 'mod_data/key', 0,0);
		
		$mod                        = str_replace("rgen_","",$mod_name);
		$mod_setting                = $this->rgen->factory->node($val, 'mod_setting', 0,0);
		$mod_setting['module_id']   = $mod_id;
		$mod_setting['module_type'] = 'gridmanager';
		$mod_setting['setting_key'] = $mod_name.'.gridmanager.'.$mod_id;

		$mod_output = $this->load->controller('rgen/'.$mod, $mod_setting);

		return $mod_output;
	}
	private function img($val){
		return str_replace("../image/","image/",$val);
	}
	private function chk($val){
		return $this->rgen->factory->checkdata($val);
	}
	private function chkstr($val, $str){
		return $this->rgen->factory->checkstr($val, $str);
	}
	private function lngdata($val) {
		$lng = $this->rgen->storage->get('language', 'language');
		return $this->rgen->factory->lngdata($val, $lng);
	}
	private function imgresize($val, $w, $h) {
		$this->load->model('tool/image');
		return $this->rgen->factory->imgresize($val, $w, $h, $this->model_tool_image);
	}


}