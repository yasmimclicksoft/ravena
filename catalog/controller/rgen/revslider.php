<?php
class ControllerRgenRevslider extends Controller {
	public function index($setting) {
		$data = array();
		$factory = $this->rgen->factory;

		/* Get module data
		------------------------*/
		if ($factory->node($setting, 'module_type', 0,0) == 'gridmanager') {
			$part = explode('.', $factory->node($setting, 'setting_key', 0,0));
		}else{
			$part = explode('.', $setting);	
		}

		if (isset($part)) {
			if ($part[1] === 'gridmanager') {
				$module_settings      = $setting;
			}else{
				$module_settings      = $this->rgen->storage->get('modules_data', $part[0].'_set');
				$module_settings      = $module_settings[$part[1]]['data'][0];	
			}
			
			$module               = $this->rgen->storage->get('modules_data', $part[0]);
			$module_common        = $module[$part[2]]['common'];
			$module_data          = $module[$part[2]]['data'];
			$module_key           = 'revslider';
			
			$data['lng']          = $this->config->get('config_language_id');
			$data['module_name']  = 'rgen-'.$module_key;
			$data['module_id']    = $part[1];
			$data['module_class'] = ' '.$module_key.'-'.$part[2];
			$data['settings']     = $module_settings;
			$data['slider_size']  = $this->rgen->factory->node($data['settings'], 'slider_size', 0,0) != '' ? $this->rgen->factory->node($data['settings'], 'slider_size', 0,0) : 'normal';
			$data['slider_type']  = $this->rgen->factory->node($data['settings'], 'slider_data/params/slider_type', 0,0);

			/* Construct module
			------------------------*/
			if (isset($module_common) && $this->chk($module_common)) {
				$data['modules'] = array();
				$data['modules']['config'] = $module_common;
				//$data['modules'] = $module_common;
			}

			/* Construct slider
			------------------------*/
			if ($this->chk($data['settings']['slider_id'])) {
				$rev_setting = array(
					'slider_id'     => $data['settings']['slider_id'],
					'revslider_key' => 'rgentheme-revo'
				);
				$data['mod_revslider'] = $this->load->controller('module/revslideroutput', $rev_setting);
			}
		}

		/* Render
		------------------------*/
		if (file_exists(DIR_TEMPLATE . DIR_FRONT_MODULE . $module_key . '.tpl')) {
			return $this->load->view(DIR_FRONT_MODULE . $module_key . '.tpl', $data);
		}
	}

	/* Helper functions
	------------------------*/
	private function chk($val){
		return $this->rgen->factory->checkdata($val);
	}
	private function lngdata($val) {
		$lng = $this->rgen->storage->get('language', 'language');
		return $this->rgen->factory->lngdata($val, $lng);
	}


}