<?php
class ControllerRgenBasicslideshow extends Controller {
	public function index($setting) {
		$data = array();
		$factory = $this->rgen->factory;

		/* Get module data
		------------------------*/
		if ($factory->node($setting, 'module_type', 0,0) == 'gridmanager') {
			$part = explode('.', $factory->node($setting, 'setting_key', 0,0));
		}else{
			$part = explode('.', $setting);	
		}

		if (isset($part)) {
			if ($part[1] === 'gridmanager') {
				$module_settings      = $setting;
			}else{
				$module_settings      = $this->rgen->storage->get('modules_data', $part[0].'_set');
				$module_settings      = $module_settings[$part[1]]['data'][0];	
			}
			
			$module               = $this->rgen->storage->get('modules_data', $part[0]);
			$module_common        = $module[$part[2]]['common'];
			$module_data          = $module[$part[2]]['data'];
			
			$data['lng']          = $this->config->get('config_language_id');
			$data['module_id']    = $part[1];
			$data['module_class'] = ' basicslideshow-'.$part[2];
			$data['settings']     = $module_settings;
						
			/* Construct module
			------------------------*/
			if (isset($module_data) && $this->chk($module_data)) {
				$data['module'] = array();
				$data['module']['config'] = $module_common;
				foreach ($module_data as $key => $value) {
					if ($value['status']) {
						$data['module']['data'][] = array(
							'slide' => $this->imgresize($value['item_data']['slide'], $data['settings']['w'], $data['settings']['h']),
							'title' => $this->lngdata($value['item_data']['title']),
							'url'   => $value['item_data']['url'],
							'win'   => $value['item_data']['win'] ? ' target="_blank"' : null
						);
					}
				}

				$this->rgen->assets->setJs("rgen/lib/SudoSlider/jquery.properload.js");
				$this->rgen->assets->setJs("rgen/lib/SudoSlider/jquery.sudoSlider.min.js");
				
			}
		}

		// Load beside slideshow position modules
		$data['pos_sidess'] = $this->load->controller('common/rgen-positions/besideslides');

		/* Render
		------------------------*/
		if (file_exists(DIR_TEMPLATE . DIR_FRONT_MODULE . 'basicslideshow.tpl')) {
			return $this->load->view(DIR_FRONT_MODULE . 'basicslideshow.tpl', $data);
		}
	}

	/* Helper functions
	------------------------*/
	private function img($val){
		return str_replace("../image/","image/",$val);
	}
	private function chk($val){
		return $this->rgen->factory->checkdata($val);
	}
	private function chkstr($val, $str){
		return $this->rgen->factory->checkstr($val, $str);
	}
	private function lngdata($val) {
		$lng = $this->rgen->storage->get('language', 'language');
		return $this->rgen->factory->lngdata($val, $lng);
	}
	private function imgresize($val, $w, $h) {
		$this->load->model('tool/image');
		return $this->rgen->factory->imgresize($val, $w, $h, $this->model_tool_image);
	}


}