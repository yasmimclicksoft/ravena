<div class="row gut-30">
	<div class="col-sm-6">
		<div class="frm-wrp">
			<h2 class="frm-hd"><?php echo $text_new_customer; ?></h2>
			<p><?php echo $text_checkout; ?></p>
			<div class="radio">
				<label>
				<?php if ($account == 'register') { ?>
				<input type="radio" name="account" value="register" checked="checked" />
				<?php } else { ?>
				<input type="radio" name="account" value="register" />
				<?php } ?>
				<?php echo $text_register; ?></label>
			</div>
			<?php if ($checkout_guest) { ?>
			<div class="radio">
				<label>
				<?php if ($account == 'guest') { ?>
				<input type="radio" name="account" value="guest" checked="checked" />
				<?php } else { ?>
				<input type="radio" name="account" value="guest" />
				<?php } ?>
				<?php echo $text_guest; ?></label>
			</div>
			<?php } ?>
			<p><?php echo $text_register_account; ?></p>
			<input type="button" value="<?php echo $button_continue; ?>" id="button-account" data-loading-text="<?php echo $text_loading; ?>" class="primary-btn" />
		</div>
	</div>

	<script type="text/javascript">
	    function validarformu(email2){
	    	var verificado = email2;
	    	corrigido = verificado.replace(/[' 'çÇáâĩũĨŨãẽõêôÔÊÂÃẼÕÁàÀéèÉÈíìÍÌóòÓÒúùÚÙñÑ!#$%*()+~}{&]/g,'');
	    	if(corrigido == email2){
	    		$("#input-email").val(corrigido);
	    		$("#erro-email2").css("display","none");
	    		console.log(email);
	    	} else {
	    		console.log(email2);
	    		$("#input-email").val("");
	    		$("#erro-email2").css("display","block");
	    	}    	
	    }
	    
    </script>

	<div class="col-sm-6">
		<div class="frm-wrp">
			<h2 class="frm-hd"><?php echo $text_returning_customer; ?></h2>
			<p><?php echo $text_i_am_returning_customer; ?></p>
			<div class="form-group">
				<label class="control-label" for="input-email"><?php echo $entry_email; ?><span id="erro-email2" style="color:#FF0000;display:none;">O email não pode ter caracteres especiais!</span></label>
				<input type="text" name="email" value="" placeholder="<?php echo $entry_email; ?>" id="input-email" onblur="validarformu(this.value)" class="form-control" />
			</div>
			<div class="form-group">
				<label class="control-label" for="input-password"><?php echo $entry_password; ?></label>
				<input type="password" name="password" value="" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control" />
				<a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a></div>
			<input type="button" value="<?php echo $button_login; ?>" id="button-login" data-loading-text="<?php echo $text_loading; ?>" class="primary-btn" />
		</div>
	</div>
</div>


