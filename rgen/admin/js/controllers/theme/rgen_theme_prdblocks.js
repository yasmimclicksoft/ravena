(function(){
	'use strict';

	angular.module('rgen').controller('themePrdblocks', [
		'$scope', 
		'Rest', 
		'Pop', 
		'Loader',
		'SweetAlert',
		function($scope, Rest, Pop, Loader, SweetAlert){

			/* Default config data
			------------------------*/
			// Theme data settings
			$scope.db = {
				prefix   : 'rgen_',
				group    : 'rgen_theme',
				settings : 'rgen_settings',
				modgroup : false,
				section  : 'prdblocks',
				suffix   : ''
			}
			
			// Default parameters
			$scope.modType    = 'new';
			$scope.modId      = null;
			$scope.themeData  = {};
			$scope.namePrefix = '';
			$scope.imgPath    = rgen_config.DIR_ADMIN_IMG;
			$scope.helpImg    = function (img) { Pop.help(img); };
			$scope.modList    = [
				{ name:'Product blocks' , val:'global_prdblocks' },
				{ name:'Small product blocks' , val:'global_smallprdblocks' },
				{ name:'Product list pages' , val:'global_prdgirdpages' }
			];

			$scope.tpl = {
				global_prdblocks      : tpl('theme/prdblocks/prdblocks'),
				global_smallprdblocks : tpl('theme/prdblocks/smallprdblocks'),
				global_prdgirdpages   : tpl('theme/prdblocks/prdgirdpages'),
				prd1                  : tpl('theme/prdblocks/prd1'),
				prd2                  : tpl('theme/prdblocks/prd2')
			}

			// Product box styles
			$scope.prdblock_tab = 1;
			$scope.prdblock_styles = [
				{ label:'Style 1', val: 1 },
				{ label:'Style 2', val: 2 }
			];
			$scope.smallprdblock_styles = [
				{ label:'Small 1', val: 1 },
				{ label:'Small 2', val: 2 }
			];
			$scope.itemtype = function (type) {
				Loader.on('.rgen-container');
				$scope.prdblock_tab = type;
				setTimeout(function(){ Loader.off('.rgen-container'); }, 200);
			}

			$scope.themeDefault = function () {
				return {
					global_prdblocks: {
						prd1: {
							status: true,
							grid: {
								cart: true,
								wishlist: true,
								compare: true,
								description: true,
								rating: true,
								price: true,
								tax: true,
								style: {}
							},
							list: {
								cart: true,
								wishlist: true,
								compare: true,
								description: true,
								rating: true,
								price: true,
								tax: true,
								style: {}
							},
							normal: {
								cart: true,
								wishlist: true,
								compare: true,
								description: true,
								rating: true,
								price: true,
								tax: true,
								style: {}
							},
							cart_ico: {},
							wish_ico: {},
							compare_ico: {},
							offer_tag: 'dis',
							offer_img: {
								img: {},
								tag_w: 40,
								tag_h: 40
							},
							offer_discount: {},
							offer_text: {
								box: {},
								text: {}
							}
						},
						prd2: {
							status: true,
							grid: {
								cart: true,
								wishlist: true,
								compare: true,
								more: true,
								description: true,
								rating: true,
								price: true,
								tax: true,
								style: {}
							},
							list: {
								cart: true,
								wishlist: true,
								compare: true,
								more: true,
								description: true,
								rating: true,
								price: true,
								tax: true,
								style: {}
							},
							normal: {
								cart: true,
								wishlist: true,
								compare: true,
								more: true,
								description: true,
								rating: true,
								price: true,
								tax: true,
								style: {}
							},
							cart_ico: {
								type: 'ico',
								icon: 'glyphicon glyphicon-shopping-cart',
							},
							wish_ico: {
								type: 'ico',
								icon: 'fa fa-heart',
							},
							compare_ico: {
								type: 'ico',
								icon: 'fa fa-retweet',
							},
							more_ico: {
								type: 'ico',
								icon: 'fa fa-arrow-circle-o-right',
							},
							offer_tag: 'dis',
							offer_img: {
								img: {},
								tag_w: 40,
								tag_h: 40
							},
							offer_discount: {},
							offer_text: {
								box: {},
								text: {}
							}
						}
					},
					global_smallprdblocks:{
						smallprd1: {},
						smallprd2: {}
					},
					global_prdgirdpages: {
						category: {
							prd_style: 1,
							gridview: {},
							listview: {}
						},
						special: {
							prd_style: 1,
							gridview: {},
							listview: {}
						},
						brand_info: {
							prd_style: 1,
							gridview: {},
							listview: {}
						},
						search: {
							prd_style: 1,
							gridview: {},
							listview: {}
						}
					}
					
				}
			}

			$scope.prd_viewtypes = 'normal';
			$scope.viewtypes = function (type) {
				$scope.prd_viewtypes = type;
			}

			$scope.prd_types = [
				{ label:'Normal', val:'normal' },
				{ label:'Grid view', val:'grid' },
				{ label:'List view', val:'list' }
			];

			$scope.prd_offertag = [
				{ label:'Image', val:'img' },
				{ label:'Discount value', val:'dis' },
				{ label:'Custom text', val:'txt' }
			];

			$scope.copySettings = function (prd, from, to) {
				Loader.on('.rgen-container');
				$scope.themeData[prd][to] = angular.copy($scope.themeData[prd][from]);
				setTimeout(function(){
					$scope.$apply(function() {
						Loader.off('.rgen-container');
					});
				}, 300);
			}

			$scope.getkey = function (id) {
				$scope.modId = null;
				$scope.modId = id;
				$scope.themeData = new $scope.themeDefault()[id];
				
				// Marge data from response
				Loader.on('.rgen-container');
				Rest.themeGetkey($scope.db.group, $scope.modId).then(function(response){
					$scope.themeData = $.extend(true, $scope.themeData, response.data);
					setTimeout(function(){
						$scope.$apply(function() {
							Loader.off('.rgen-container');	
						});
					}, 200);
				}, function (error){
					Pop.pop_error(error);
				});
				//=========================
			}
			$scope.getkey($scope.modList[0].val);

			// Reseting data
			$scope.reset     = function (id) { Pop.reset($scope, id); }
			$scope.resetData = function (id) {
				switch(id) {
					
					// Prd 1 reset
					case 1:
					$scope.themeData.prd1 = new $scope.themeDefault()['global_prdblocks']['prd1'];
					break;

					// Prd 2 reset
					case 2:
					$scope.themeData.prd2 = new $scope.themeDefault()['global_prdblocks']['prd2'];
					break;

					case 'global_prdgirdpages':
					$scope.themeData = new $scope.themeDefault()['global_prdgirdpages'];
					break;

					case 'global_smallprdblocks':
					$scope.themeData = new $scope.themeDefault()['global_smallprdblocks'];
					break;

					case 'smallprd1':
					$scope.themeData.smallprd1 = new $scope.themeDefault()['global_smallprdblocks']['smallprd1'];
					break;
				}
			}
			
			$scope.fontObj = function (id) {
				switch(id) {
					case 'global_prdblocks':
						var prd1 = $scope.themeData.prd1;
						var prd2 = $scope.themeData.prd2;
						return _.object([
							['prd1_offer_dis'        , Rest.objChk(prd1, 'offer_discount') ? Rest.findFonts(prd1.offer_discount) : null ],
							['prd1_offer_txt'        , Rest.objChk(prd1, 'offer_text.box') ? Rest.findFonts(prd1.offer_text.box) : null ],

							['prd1_normal_name'        , Rest.objChk(prd1, 'normal.style.prd.info_name') ? Rest.findFonts(prd1.normal.style.prd.info_name) : null ],
							['prd1_normal_description' , Rest.objChk(prd1, 'normal.style.prd.info_description') ? Rest.findFonts(prd1.normal.style.prd.info_description) : null ],
							['prd1_normal_price_spl'   , Rest.objChk(prd1, 'normal.style.prd.price_spl') ? Rest.findFonts(prd1.normal.style.prd.price_spl) : null ],
							['prd1_normal_price_new'   , Rest.objChk(prd1, 'normal.style.prd.price_new') ? Rest.findFonts(prd1.normal.style.prd.price_new) : null ],
							['prd1_normal_cartbtn'     , Rest.objChk(prd1, 'normal.style.prd.cart.btn') ? Rest.findFonts(prd1.normal.style.prd.cart.btn) : null ],

							['prd1_grid_name'        , Rest.objChk(prd1, 'grid.style.prd.info_name') ? Rest.findFonts(prd1.grid.style.prd.info_name) : null ],
							['prd1_grid_description' , Rest.objChk(prd1, 'grid.style.prd.info_description') ? Rest.findFonts(prd1.grid.style.prd.info_description) : null ],
							['prd1_grid_price_spl'   , Rest.objChk(prd1, 'grid.style.prd.price_spl') ? Rest.findFonts(prd1.grid.style.prd.price_spl) : null ],
							['prd1_grid_price_new'   , Rest.objChk(prd1, 'grid.style.prd.price_new') ? Rest.findFonts(prd1.grid.style.prd.price_new) : null ],
							['prd1_grid_cartbtn'     , Rest.objChk(prd1, 'grid.style.prd.cart.btn') ? Rest.findFonts(prd1.grid.style.prd.cart.btn) : null ],

							['prd1_list_name'        , Rest.objChk(prd1, 'list.style.prd.info_name') ? Rest.findFonts(prd1.list.style.prd.info_name) : null ],
							['prd1_list_description' , Rest.objChk(prd1, 'list.style.prd.info_description') ? Rest.findFonts(prd1.list.style.prd.info_description) : null ],
							['prd1_list_price_spl'   , Rest.objChk(prd1, 'list.style.prd.price_spl') ? Rest.findFonts(prd1.list.style.prd.price_spl) : null ],
							['prd1_list_price_new'   , Rest.objChk(prd1, 'list.style.prd.price_new') ? Rest.findFonts(prd1.list.style.prd.price_new) : null ],
							['prd1_list_cartbtn'     , Rest.objChk(prd1, 'list.style.prd.cart.btn') ? Rest.findFonts(prd1.list.style.prd.cart.btn) : null ],
							
							/*----------------*/

							['prd2_offer_dis'        , Rest.objChk(prd2, 'offer_discount') ? Rest.findFonts(prd2.offer_discount) : null ],
							['prd2_offer_txt'        , Rest.objChk(prd2, 'offer_text.box') ? Rest.findFonts(prd2.offer_text.box) : null ],

							['prd2_normal_name'        , Rest.objChk(prd2, 'normal.style.prd.info_name') ? Rest.findFonts(prd2.normal.style.prd.info_name) : null ],
							['prd2_normal_description' , Rest.objChk(prd2, 'normal.style.prd.info_description') ? Rest.findFonts(prd2.normal.style.prd.info_description) : null ],
							['prd2_normal_price_spl'   , Rest.objChk(prd2, 'normal.style.prd.price_spl') ? Rest.findFonts(prd2.normal.style.prd.price_spl) : null ],
							['prd2_normal_price_new'   , Rest.objChk(prd2, 'normal.style.prd.price_new') ? Rest.findFonts(prd2.normal.style.prd.price_new) : null ],
							['prd2_normal_cartbtn'     , Rest.objChk(prd2, 'normal.style.prd.cart.btn') ? Rest.findFonts(prd2.normal.style.prd.cart.btn) : null ],

							['prd2_grid_name'        , Rest.objChk(prd2, 'grid.style.prd.info_name') ? Rest.findFonts(prd2.grid.style.prd.info_name) : null ],
							['prd2_grid_description' , Rest.objChk(prd2, 'grid.style.prd.info_description') ? Rest.findFonts(prd2.grid.style.prd.info_description) : null ],
							['prd2_grid_price_spl'   , Rest.objChk(prd2, 'grid.style.prd.price_spl') ? Rest.findFonts(prd2.grid.style.prd.price_spl) : null ],
							['prd2_grid_price_new'   , Rest.objChk(prd2, 'grid.style.prd.price_new') ? Rest.findFonts(prd2.grid.style.prd.price_new) : null ],
							['prd2_grid_cartbtn'     , Rest.objChk(prd2, 'grid.style.prd.cart.btn') ? Rest.findFonts(prd2.grid.style.prd.cart.btn) : null ],

							['prd2_list_name'        , Rest.objChk(prd2, 'list.style.prd.info_name') ? Rest.findFonts(prd2.list.style.prd.info_name) : null ],
							['prd2_list_description' , Rest.objChk(prd2, 'list.style.prd.info_description') ? Rest.findFonts(prd2.list.style.prd.info_description) : null ],
							['prd2_list_price_spl'   , Rest.objChk(prd2, 'list.style.prd.price_spl') ? Rest.findFonts(prd2.list.style.prd.price_spl) : null ],
							['prd2_list_price_new'   , Rest.objChk(prd2, 'list.style.prd.price_new') ? Rest.findFonts(prd2.list.style.prd.price_new) : null ],
							['prd2_list_cartbtn'     , Rest.objChk(prd2, 'list.style.prd.cart.btn') ? Rest.findFonts(prd2.list.style.prd.cart.btn) : null ],
							
						]);
					break;
					case 'global_smallprdblocks':
						return _.object([
							['smallprd1_name'     , Rest.objChk($scope.themeData, 'smallprd1.prd.prdname') ? Rest.findFonts($scope.themeData.smallprd1.prd.prdname) : null ],
							['smallprd2_name'     , Rest.objChk($scope.themeData, 'smallprd2.prd.prdname') ? Rest.findFonts($scope.themeData.smallprd2.prd.prdname) : null ]
						]);
					break;
				}
			}
			
			$scope.settingsObj = function (id) {

				switch(id) {
					case 'global_prdblocks':
						var prd1 = $scope.themeData.prd1;
						var prd2 = $scope.themeData.prd2;
						return {
							gb_prd1: {
								grid: {
									cart               : Rest.getVal(prd1, 'grid.cart', true),
									wishlist           : Rest.getVal(prd1, 'grid.wishlist', true),
									compare            : Rest.getVal(prd1, 'grid.compare', true),
									description        : Rest.getVal(prd1, 'grid.description', true),
									rating             : Rest.getVal(prd1, 'grid.rating', true),
									price              : Rest.getVal(prd1, 'grid.price', true),
									tax                : Rest.getVal(prd1, 'grid.tax', true)
								},
								list: {
									cart               : Rest.getVal(prd1, 'list.cart', true),
									wishlist           : Rest.getVal(prd1, 'list.wishlist', true),
									compare            : Rest.getVal(prd1, 'list.compare', true),
									description        : Rest.getVal(prd1, 'list.description', true),
									rating             : Rest.getVal(prd1, 'list.rating', true),
									price              : Rest.getVal(prd1, 'list.price', true),
									tax                : Rest.getVal(prd1, 'list.tax', true)
								},
								normal: {
									cart               : Rest.getVal(prd1, 'normal.cart', true),
									wishlist           : Rest.getVal(prd1, 'normal.wishlist', true),
									compare            : Rest.getVal(prd1, 'normal.compare', true),
									description        : Rest.getVal(prd1, 'normal.description', true),
									rating             : Rest.getVal(prd1, 'normal.rating', true),
									price              : Rest.getVal(prd1, 'normal.price', true),
									tax                : Rest.getVal(prd1, 'normal.tax', true)
								},
								common: {
									cart_ico_status    : Rest.getVal(prd1, 'cart_ico.status', true),
									cart_ico_type      : Rest.getVal(prd1, 'cart_ico.type', 'ico'),
									cart_ico           : Rest.getVal(prd1, 'cart_ico.icon', 'glyphicon glyphicon-shopping-cart'),
									wish_ico_status    : Rest.getVal(prd1, 'wish_ico.status', true),
									wish_ico_type      : Rest.getVal(prd1, 'wish_ico.type', 'ico'),
									wish_ico           : Rest.getVal(prd1, 'wish_ico.icon', 'fa fa-heart'),
									compare_ico_status : Rest.getVal(prd1, 'compare_ico.status', true),
									compare_ico_type   : Rest.getVal(prd1, 'compare_ico.type', 'ico'),
									compare_ico        : Rest.getVal(prd1, 'compare_ico.icon', 'fa fa-exchange'),
									offer_tag          : Rest.getVal(prd1, 'offer_tag', 'img'),
									offer_text         : Rest.getVal(prd1, 'offer_text.text', 'Offer')
								}
								
							},
							gb_prd2: {
								grid: {
									cart        : Rest.getVal(prd2, 'grid.cart', true),
									wishlist    : Rest.getVal(prd2, 'grid.wishlist', true),
									compare     : Rest.getVal(prd2, 'grid.compare', true),
									more        : Rest.getVal(prd2, 'grid.more', true),
									description : Rest.getVal(prd2, 'grid.description', true),
									rating      : Rest.getVal(prd2, 'grid.rating', true),
									price       : Rest.getVal(prd2, 'grid.price', true),
									tax         : Rest.getVal(prd2, 'grid.tax', true)
								},
								list: {
									cart        : Rest.getVal(prd2, 'list.cart', true),
									wishlist    : Rest.getVal(prd2, 'list.wishlist', true),
									compare     : Rest.getVal(prd2, 'list.compare', true),
									more        : Rest.getVal(prd2, 'list.more', true),
									description : Rest.getVal(prd2, 'list.description', true),
									rating      : Rest.getVal(prd2, 'list.rating', true),
									price       : Rest.getVal(prd2, 'list.price', true),
									tax         : Rest.getVal(prd2, 'list.tax', true)
								},
								normal: {
									cart        : Rest.getVal(prd2, 'normal.cart', true),
									wishlist    : Rest.getVal(prd2, 'normal.wishlist', true),
									compare     : Rest.getVal(prd2, 'normal.compare', true),
									more        : Rest.getVal(prd2, 'normal.more', true),
									description : Rest.getVal(prd2, 'normal.description', true),
									rating      : Rest.getVal(prd2, 'normal.rating', true),
									price       : Rest.getVal(prd2, 'normal.price', true),
									tax         : Rest.getVal(prd2, 'normal.tax', true)
								},
								common: {
									cart_ico_status    : Rest.getVal(prd2, 'cart_ico.status', true),
									cart_ico_type      : Rest.getVal(prd2, 'cart_ico.type', 'ico'),
									cart_ico           : Rest.getVal(prd2, 'cart_ico.icon', 'glyphicon glyphicon-shopping-cart'),
									wish_ico_status    : Rest.getVal(prd2, 'wish_ico.status', true),
									wish_ico_type      : Rest.getVal(prd2, 'wish_ico.type', 'ico'),
									wish_ico           : Rest.getVal(prd2, 'wish_ico.icon', 'fa fa-heart'),
									compare_ico_status : Rest.getVal(prd2, 'compare_ico.status', true),
									compare_ico_type   : Rest.getVal(prd2, 'compare_ico.type', 'ico'),
									compare_ico        : Rest.getVal(prd2, 'compare_ico.icon', 'fa fa-exchange'),
									more_ico_status    : Rest.getVal(prd2, 'more_ico.status', true),
									more_ico_type      : Rest.getVal(prd2, 'more_ico.type', 'ico'),
									more_ico           : Rest.getVal(prd2, 'more_ico.icon', 'fa fa-arrow-circle-o-right'),
									offer_tag          : Rest.getVal(prd2, 'offer_tag', 'img'),
									offer_text         : Rest.getVal(prd2, 'offer_text.text', 'Offer')
								}
							}
						};
					break;

					case 'global_prdgirdpages':
						return {
							category: {
								prd_style : Rest.getVal($scope.themeData.category, 'prd_style', 1),
								gridview  : Rest.getVal($scope.themeData.category, 'gridview.classGroup', 'eq4 d-eq4 t-eq3 mxl-eq2 msm-eq2 mxs-eq1 gt20 mb20'),
								listview  : Rest.getVal($scope.themeData.category, 'listview.classGroup', 'eq1 gt0 mb20')
							},
							special: {
								prd_style : Rest.getVal($scope.themeData.special, 'prd_style', 1),
								gridview  : Rest.getVal($scope.themeData.special, 'gridview.classGroup', 'eq4 d-eq4 t-eq3 mxl-eq2 msm-eq2 mxs-eq1 gt20 mb20'),
								listview  : Rest.getVal($scope.themeData.special, 'listview.classGroup', 'eq1 gt0 mb20')
							},
							brand_info: {
								prd_style : Rest.getVal($scope.themeData.brand_info, 'prd_style', 1),
								gridview  : Rest.getVal($scope.themeData.brand_info, 'gridview.classGroup', 'eq4 d-eq4 t-eq3 mxl-eq2 msm-eq2 mxs-eq1 gt20 mb20'),
								listview  : Rest.getVal($scope.themeData.brand_info, 'listview.classGroup', 'eq1 gt0 mb20')
							},
							search: {
								prd_style : Rest.getVal($scope.themeData.search, 'prd_style', 1),
								gridview  : Rest.getVal($scope.themeData.search, 'gridview.classGroup', 'eq4 d-eq4 t-eq3 mxl-eq2 msm-eq2 mxs-eq1 gt20 mb20'),
								listview  : Rest.getVal($scope.themeData.search, 'listview.classGroup', 'eq1 gt0 mb20')
							}
						};
					break;

				}
			}
			
			$scope.save = function(){
				// Fonts
				$scope.fonts = $scope.fontObj($scope.modId) ? $scope.fontObj($scope.modId) : {};

				// Settings
				$scope.settings = $scope.settingsObj($scope.modId) ? $scope.settingsObj($scope.modId) : {};

				// Save data
				Loader.on('.rgen-container');
				// $group, $section, $key, $value
				Rest.themeEditkey($scope.db.group, $scope.db.prefix+$scope.db.section, $scope.modId, $scope.themeData).then(function(){
					
					if (_.size($scope.settings) > 0) {
						Rest.settingsSave($scope.db.settings, $scope.db.prefix+$scope.db.section, $scope.settings);
					}

					if (_.size($scope.fonts) > 0) {
						// Save fonts
						Rest.themeSavefonts($scope.db.group, 'fonts', $scope.fonts).then(function(){
							Pop.pop_success(rgen_config.text_success);
							Loader.off('.rgen-container');
						}, function (error){
							Loader.off('.rgen-container');
							Pop.pop_error(error);
						});
					} else {
						Pop.pop_success(rgen_config.text_success);
						Loader.off('.rgen-container');
					};

				}, function (error){
					Loader.off('.rgen-container');
					Pop.pop_error(error);
				});
			}


	}]);

})();