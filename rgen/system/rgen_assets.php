<?php 
class assets {
	public $cssFiles = array();
	public $jsFiles = array();

	public $admin_cssFiles = array();
	public $admin_jsFiles = array();
	
	public function __construct($registry) {
		$url_arg = "?ver=" . rand(1, 9999);

		//$assets_data = json_decode(file_get_contents(DIR_SYSTEM.'../rgen/system/assets_data.json'));
		$assets_data = json_decode(file_get_contents(FILE_FRONT_ASSETS));
		foreach ($assets_data->css as $key => $value) {
			$this->cssFiles[md5($value)] = !factory::checkstr($value, 'fonts.googleapis.com/') ? $value : $value;
		}
		foreach ($assets_data->js as $key => $value) {
			$this->jsFiles[md5($value)] = $value;
		}

		$admin_assets_data = json_decode(file_get_contents(FILE_ADMIN_ASSETS));
		foreach ($admin_assets_data->css as $key => $value) {
			$this->admin_cssFiles[$key] = !factory::checkstr($value, 'fonts.googleapis.com/') ? $value : $value;
		}
		foreach ($admin_assets_data->js as $key => $value) {
			$this->admin_jsFiles[$key] = $value;
		}
	}
	
	public function setCss($v){
		if ($v != 'catalog/view/javascript/jquery/owl-carousel/owl.carousel.css') {
			$this->cssFiles[md5($v)] = $v;
		}
	}
	public function setJs($v){
		if ($v != 'catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js') {
			$this->jsFiles[md5($v)] = $v;
		}
	}
	public function js(){

	}
	public function css(){

	}
	public function gfonts($val){
		if (factory::checkdata($val)) {
			$fonts = $val;
			$f = '';
			$s = '';
			foreach ($fonts as $key => $value) {
				if (isset($value['family']) && $value['family'] != '') {
					$f[md5($value['family'])] = $value['family'];
				}
				if (isset($value['subset']) && $value['subset'] != '') {
					$s[md5($value['subset'])] = $value['subset'];
				}
			}
			$f_url 	= !factory::isEmpty($f) && $f != '' ? join($f, '|') : '';
			$s_url 	= !factory::isEmpty($s) && $s != '' ? '&subset='.join($s, ',') : '';
			$gfont_url = $f_url != '' && $s_url != '' ? "@import url('//fonts.googleapis.com/css?family=".$f_url.$s_url."');" : '';
			return $gfont_url;
		} else {
			return false;
		}
	}
}?>