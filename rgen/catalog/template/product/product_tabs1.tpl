<div class="nav-tabs1">
	<ul class="nav nav-tabs">
		<li class="active"><a href="#tab-options" data-toggle="tab"><?php echo $prd_option_tab; ?></a></li>
		<li><a href="#tab-description" data-toggle="tab"><?php echo $tab_description; ?></a></li>
		<?php if ($attribute_groups) { ?>
		<li><a href="#tab-specification" data-toggle="tab"><?php echo $tab_attribute; ?></a></li>
		<?php } ?>
		<?php if ($review_status) { ?>
		<li data-toggle="tooltip" title="<?php echo $tab_review; ?>"><a href="#tab-review" data-toggle="tab"><i class="fa fa-comment"></i></a></li>
		<?php } ?>
	</ul>
	<div class="tab-content">
		<?php 
		/* OPTIONS TAB
		**************************/ ?>
		<div class="tab-pane active" id="tab-options">
			<?php include $this->rgen('prd_options'); ?>
		</div>
		
		<?php 
		/* DESCRIPTION TAB
		**************************/ ?>
		<div class="tab-pane" id="tab-description"><?php echo $description; ?></div>
		
		<?php 
		/* SPECIFICATION TAB
		**************************/ ?>
		<?php if ($attribute_groups) { ?>
		<div class="tab-pane" id="tab-specification">
			<table class="table table-bordered">
				<?php foreach ($attribute_groups as $attribute_group) { ?>
				<thead>
					<tr>
						<td colspan="2"><strong><?php echo $attribute_group['name']; ?></strong></td>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($attribute_group['attribute'] as $attribute) { ?>
					<tr>
						<td><?php echo $attribute['name']; ?></td>
						<td><?php echo $attribute['text']; ?></td>
					</tr>
					<?php } ?>
				</tbody>
				<?php } ?>
			</table>
		</div>
		<?php } ?>
		
		<?php 
		/* REIVEW TAB
		**************************/ ?>
		<?php if ($review_status) { ?>
		<div class="tab-pane" id="tab-review">
			<form id="form-review">
				<div id="review"></div>
				<h4 class="h4 bdr-b"><?php echo $text_write; ?></h4>
				<div class="form-group required">
					<label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
					<input type="text" name="name" value="" id="input-name" class="form-control" />
				</div>
				<div class="form-group required">
					<label class="control-label" for="input-review"><?php echo $entry_review; ?></label>
					<textarea name="text" rows="5" id="input-review" class="form-control"></textarea>
					<div class="help-block"><?php echo $text_note; ?></div>
				</div>
				<div class="form-group required">
					<label class="control-label"><?php echo $entry_rating; ?></label>
					<ul class="rating ul-reset">
						<li><?php echo $entry_bad; ?></li>
						<li class="rate-count">
							<input type="radio" name="rating" value="1" />
							<input type="radio" name="rating" value="2" />
							<input type="radio" name="rating" value="3" />
							<input type="radio" name="rating" value="4" />
							<input type="radio" name="rating" value="5" />	
						</li>
						<li><?php echo $entry_good; ?></li>
					</ul>
				</div>
				<?php if ($site_key) { ?>
				<div class="form-group">
					<div class="col-sm-12">
						<div class="g-recaptcha" data-sitekey="<?php echo $site_key; ?>"></div>
					</div>
				</div>
				<?php } ?>
				<button type="button" id="button-review" data-loading-text="<?php echo $text_loading; ?>" class="btn default-btn"><?php echo $button_continue; ?></button>
				
			</form>
		</div>
		<?php } ?>

	</div>
</div>

