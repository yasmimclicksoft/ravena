<div class="buying-info">
	<div class="price-wrp">
		<?php 
		/* PRICE DATA
		**************************/
		if ($price) { ?>
		<div class="price vm">
			<b>
				<?php 
				/* Price */
				if (!$special) { ?>
				<span class="price-new"><?php echo $price; ?></span>
				<?php } else { ?>
				<span class="price-old"><?php echo $price; ?></span>
				<span class="price-new price-spl"><?php echo $special; ?></span>
				<?php } ?>
				<?php 
				/* TAX */
				if ($tax) { ?>
				<span class="price-tax"><?php echo $text_tax; ?> <?php echo $tax; ?></span>
				<?php } ?>	
			</b>
		</div>
			<?php 
			/* Points */
			if ($points) { ?>
			<span class="reward"><?php echo $text_points; ?> <?php echo $points; ?></span>
			<?php } ?>

			<?php
			/* Discount */
			if ($discounts) { ?>
			<ul class="discount ul-reset">
				<?php foreach ($discounts as $discount) { ?>
				<li><?php echo $discount['quantity']; ?><?php echo $text_discount; ?><?php echo $discount['price']; ?></li>
				<?php } ?>
			</ul>
			<?php } ?>

		<?php } ?>
	</div>

	<?php 
	/* PRODUCT QUANTITY BOX
	**************************/ ?>
	<div class="buy-btn-wrp">
		<label class="control-label" for="input-quantity"><?php echo $entry_qty; ?></label>
		<div class="control-qty">
			<a class="qty-handle" onclick="qtyMinus();"><i class="fa fa-minus-circle"></i></a>
			<input type="text" name="quantity" value="<?php echo $minimum; ?>" size="2" id="input-quantity" class="form-control" />
			<a class="qty-handle" onclick="qtyPlus();"><i class="fa fa-plus-circle"></i></a>
			<input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
		</div>
		<button type="button" class="btn btn-cart<?php echo $carttxt; ?>" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" ><i class="<?php echo $cart_ico; ?>"></i><?php echo $button_cart; ?></button>
		<div class="cart-option">
			<a onclick="wishlist.add('<?php echo $product_id; ?>');"><?php echo $button_wishlist; ?></a>
			<a onclick="compare.add('<?php echo $product_id; ?>');"><?php echo $button_compare; ?></a>	
		</div>
	</div>
	<?php if ($minimum > 1) { ?>
	<div class="min-qty"><?php echo $text_minimum; ?></div>
	<?php } ?>
</div>