<?php 
if ($prdpg == 1 || $prdpg == 2) {?>
	<ul class="ul-reset item-info">
		<?php if ($manufacturer) { ?>
		<li><span><?php echo $text_manufacturer; ?></span> <a href="<?php echo $manufacturers; ?>"><?php echo $manufacturer; ?></a></li>
		<?php } ?>
		<li><span><?php echo $text_model; ?></span> <?php echo $model; ?></li>
		<?php if ($reward) { ?>
		<li><span><?php echo $text_reward; ?></span> <?php echo $reward; ?></li>
		<?php } ?>
		<li><span><?php echo $text_stock; ?></span> <?php echo $stock; ?></li>
	</ul>

	<?php 
	/* REIVEW
	**************************/ ?>
	<?php if ($review_status) { ?>
	<div class="rating-wrp">
		<span class="review-stars large" data-toggle="tooltip" title="<?php echo '('.$reviews.')'; ?>">
			<?php for ($i = 1; $i <= 5; $i++) { ?>
			<?php if ($rating < $i) { ?>
			<i class="fa fa-star-o"></i>
			<?php } else { ?>
			<i class="fa fa-star"></i>
			<?php } ?>
			<?php } ?>	
		</span>
		<span class="review-links">
			<a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?php echo $reviews; ?></a><br>
			<a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?php echo $text_write; ?></a>	
		</span>
		
	</div>
	<?php } ?>
<?php } ?>

<?php 
/* PRODUCT OPTIONS
**************************/ ?>
<?php if ($options || $recurrings) { ?>
<div class="options-wrp">
	<?php foreach ($options as $option) { ?>
		
		<?php if ($option['type'] == 'select') { ?>
		<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
			<label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
			<div class="fields">
				<select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
					<option value=""><?php echo $text_select; ?></option>
					<?php foreach ($option['product_option_value'] as $option_value) { ?>
					<option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
					<?php if ($option_value['price']) { ?>
					(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
					<?php } ?>
					</option>
					<?php } ?>
				</select>
			</div>
		</div>
		<?php } ?>

		<?php if ($option['type'] == 'radio') { ?>
		<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
			<label class="control-label"><?php echo $option['name']; ?></label>
			<div class="fields">
				<div id="input-option<?php echo $option['product_option_id']; ?>">
					<?php foreach ($option['product_option_value'] as $option_value) { ?>
					<div class="radio">
						<label>
							<input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
							<?php echo $option_value['name']; ?>
							<?php if ($option_value['price']) { ?>
							(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
							<?php } ?>
						</label>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
		<?php } ?>


		<?php if ($option['type'] == 'checkbox') { ?>
		<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
			<label class="control-label"><?php echo $option['name']; ?></label>
			<div class="fields">
				<div id="input-option<?php echo $option['product_option_id']; ?>">
				<?php foreach ($option['product_option_value'] as $option_value) { ?>
				<div class="checkbox">
					<label>
						<input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" />
						<?php echo $option_value['name']; ?>
						<?php if ($option_value['price']) { ?>
						(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
						<?php } ?>
					</label>
				</div>
				<?php } ?>
				</div>	
			</div>
		</div>
		<?php } ?>

		<?php if ($option['type'] == 'image') { ?>
		<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
			<label class="control-label"><?php echo $option['name']; ?></label>
			<div class="fields">
				<div id="input-option<?php echo $option['product_option_id']; ?>">
					<?php foreach ($option['product_option_value'] as $option_value) { ?>
					<div class="radio">
					<label>
					<input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
					<img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> <?php echo $option_value['name']; ?>
					<?php if ($option_value['price']) { ?>
					(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
					<?php } ?>
					</label>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
		<?php } ?>

		<?php if ($option['type'] == 'text') { ?>
		<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
			<label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
			<div class="fields">
				<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />	
			</div>
		</div>
		<?php } ?>

		<?php if ($option['type'] == 'textarea') { ?>
		<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
			<label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
			<div class="fields">
				<textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
			</div>
		</div>
		<?php } ?>

		<?php if ($option['type'] == 'file') { ?>
		<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
			<label class="control-label"><?php echo $option['name']; ?></label>
			<div class="fields">
				<button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn default-btn btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
				<input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />	
			</div>
		</div>
		<?php } ?>

		<?php if ($option['type'] == 'date') { ?>
		<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
			<label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
			<div class="fields">
				<div class="input-group date">
					<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
					<span class="input-group-btn"><button class="btn default-btn" type="button"><i class="fa fa-calendar"></i></button></span>
				</div>
			</div>
		</div>
		<?php } ?>

		<?php if ($option['type'] == 'datetime') { ?>
		<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
			<label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
			<div class="fields">
				<div class="input-group datetime">
					<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
					<span class="input-group-btn"><button type="button" class="btn default-btn"><i class="fa fa-calendar"></i></button></span>
				</div>
			</div>
		</div>
		<?php } ?>

		<?php if ($option['type'] == 'time') { ?>
		<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
			<label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
			<div class="fields">
				<div class="input-group time">
					<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
					<span class="input-group-btn"><button type="button" class="btn default-btn"><i class="fa fa-calendar"></i></button></span>
				</div>
			</div>
		</div>
		<?php } ?>

	<?php } ?>

	<?php if ($recurrings) { ?>
	<h3><?php echo $text_payment_recurring ?></h3>
	<div class="form-group required">
		<select name="recurring_id" class="form-control">
			<option value=""><?php echo $text_select; ?></option>
			<?php foreach ($recurrings as $recurring) { ?>
			<option value="<?php echo $recurring['recurring_id'] ?>"><?php echo $recurring['name'] ?></option>
			<?php } ?>
		</select>
		<div class="help-block" id="recurring-description"></div>
	</div>
	<?php } ?>
</div>
<?php } ?>

<?php 
if ($prdpg == 2 || $prdpg == 3) {
/* PRODUCT QUANTITY BOX
**************************/ ?>
<div class="buy-btn-wrp1">
	<label class="control-label" for="input-quantity"><?php echo $entry_qty; ?></label>
	<div class="control-qty">
		<a class="qty-handle" onclick="qtyMinus();"><i class="fa fa-minus-circle"></i></a>
		<input type="text" name="quantity" value="<?php echo $minimum; ?>" size="2" id="input-quantity" class="form-control" />
		<a class="qty-handle" onclick="qtyPlus();"><i class="fa fa-plus-circle"></i></a>
		<input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
	</div>
	<button type="button" class="btn btn-cart<?php echo $carttxt; ?>" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" ><i class="<?php echo $cart_ico; ?>"></i><?php echo $button_cart; ?></button>
	<div class="cart-option">
		<a onclick="wishlist.add('<?php echo $product_id; ?>');"><?php echo $button_wishlist; ?></a>
		<a onclick="compare.add('<?php echo $product_id; ?>');"><?php echo $button_compare; ?></a>	
	</div>
	<?php if ($minimum > 1) { ?>
	<div class="min-qty"><?php echo $text_minimum; ?></div>
	<?php } ?>
</div>
<?php } ?>



<!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_default_style">
	<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a> <a class="addthis_button_tweet"></a> <a class="addthis_button_pinterest_pinit"></a> <a class="addthis_counter addthis_pill_style"></a>
</div>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-515eeaf54693130e"></script> 
<!-- AddThis Button END --> 
