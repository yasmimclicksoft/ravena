<div class="bannerimg-block bannerimg-block1<?php echo $banner['margin_b']; ?>">
	<?php if (isset($banner['url']) && $banner['url'] != '') { ?>
	<a class="image" href="<?php echo $banner['url']; ?>"<?php echo $banner['open'] == 'y' ? " target='_blank'" : null; ?>>
		<img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title'][$lng]; ?>" title="<?php echo $banner['title'][$lng]; ?>" class="img-responsive" />
		<span class="name"><?php echo $banner['title'][$lng]; ?></span>
	</a>
	<?php } else { ?>
	<span class="image">
		<img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title'][$lng]; ?>" title="<?php echo $banner['title'][$lng]; ?>" class="img-responsive" />
		<span class="name"><?php echo $banner['title'][$lng]; ?></span>
	</span>
	<?php } ?>
</div>