<div class="bannerimg-block bannerimg-block3<?php echo $banner['margin_b']; ?>">
	<?php if (isset($banner['url']) && $banner['url'] != '') { ?>
	<a class="image" href="<?php echo $banner['url']; ?>"<?php echo $banner['open'] == 'y' ? " target='_blank'" : null; ?>>
		<img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title'][$lng]; ?>" title="<?php echo $banner['title'][$lng]; ?>" class="img-responsive" />
		<span class="name"><b><?php echo $banner['title'][$lng]; ?></b></span>
	</a>
	<?php } else { ?>
	<span class="image">
		<img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title'][$lng]; ?>" title="<?php echo $banner['title'][$lng]; ?>" class="img-responsive" />
		<span class="name"><b><?php echo $banner['title'][$lng]; ?></b></span>
	</span>
	<?php } ?>
</div>