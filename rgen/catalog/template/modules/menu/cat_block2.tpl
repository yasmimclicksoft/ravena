<ul class="mg-items cat-block2">
	<li class="cat-img"><img src="<?php echo $item['item_data']['image']; ?>" alt="<?php echo $item['item_data']['title']; ?>"></li>
	<li class="hd"><a class="sub-item" href="<?php echo $item['item_data']['url']; ?>" data-image="<?php echo $item['item_data']['image']; ?>"><?php echo $item['item_data']['title']; ?></a></li>
	<?php 
	$i = 1;
	if (isset($item['nested_item'])) { 
	foreach ($item['nested_item'] as $k => $v) {
	if ($i <= $item['item_data']['max_sub']) { ?>
	<li class="cat-link">
		<a class="sub-item" href="<?php echo $v['item_data']['url']; ?>" data-image="<?php echo $v['item_data']['image']; ?>"><?php echo $v['item_data']['title']; ?></a>
		<?php if (isset($v['navfly'])) { echo $v['navfly']; } ?>
	</li>
	<?php } $i++; } } ?>
</ul>