<div 
	class="banner-grid-item" 
	id="<?php echo 'banner_grid_block_'.$g; ?>" 
	data-colspan="<?php echo $block['setting']['w']; ?>" 
	data-rowspan="<?php echo $block['setting']['h']; ?>">

	<?php 
	switch ($block['setting']['hoverstyle']) {
		case '1': ?>
			<div class="effect-1 h-effect">
				<img src="<?php echo $block['images']['img']; ?>" alt="<?php echo $block['images']['img_alt'][$lng]; ?>" class="img-responsive" />
				<figcaption>
					<div>
						<h2 class="h3"><?php echo html_entity_decode($block['title'][$lng], ENT_QUOTES, 'UTF-8'); ?></h2>
						<p><?php echo html_entity_decode($block['description'][$lng], ENT_QUOTES, 'UTF-8'); ?></p>
					</div>
					<?php if ($block['images']['url'] != '') { ?>
					<a href="<?php echo $block['images']['url']; ?>"<?php echo $block['images']['win']; ?>>
					<?php echo html_entity_decode($block['btn_text'][$lng], ENT_QUOTES, 'UTF-8'); ?>
					</a>
					<?php } ?>
				</figcaption>
			</div>
		<?php break;

		case '2': ?>
			<figure class="effect-2 h-effect">
				<img src="<?php echo $block['images']['img']; ?>" alt="<?php echo $block['images']['img_alt'][$lng]; ?>" class="img-responsive" />
				<figcaption>
					<h2 class="h3"><?php echo html_entity_decode($block['title'][$lng], ENT_QUOTES, 'UTF-8'); ?></h2>
					<p><?php echo html_entity_decode($block['description'][$lng], ENT_QUOTES, 'UTF-8'); ?></p>
					<?php if ($block['images']['url'] != '') { ?>
					<a href="<?php echo $block['images']['url']; ?>"<?php echo $block['images']['win']; ?>>
					<?php echo html_entity_decode($block['btn_text'][$lng], ENT_QUOTES, 'UTF-8'); ?>
					</a>
					<?php } ?>
				</figcaption>
			</figure>
		<?php break;

		case '3': ?>
			<figure class="effect-3 h-effect">
				<img src="<?php echo $block['images']['img']; ?>" alt="<?php echo $block['images']['img_alt'][$lng]; ?>" class="img-responsive" />
				<figcaption>
					<h2 class="h3"><?php echo html_entity_decode($block['title'][$lng], ENT_QUOTES, 'UTF-8'); ?></h2>
					<p><?php echo html_entity_decode($block['description'][$lng], ENT_QUOTES, 'UTF-8'); ?></p>
					<?php if ($block['images']['url'] != '') { ?>
					<a href="<?php echo $block['images']['url']; ?>"<?php echo $block['images']['win']; ?>>
					<?php echo html_entity_decode($block['btn_text'][$lng], ENT_QUOTES, 'UTF-8'); ?>
					</a>
					<?php } ?>
				</figcaption>
			</figure>
		<?php break;

		case '4': ?>
			<figure class="effect-4 h-effect">
				<?php if ($block['images']['url'] != '') { ?>
				<a href="<?php echo $block['images']['url']; ?>"<?php echo $block['images']['win']; ?>><img src="image/spacer.gif" alt="<?php echo $block['images']['img_alt'][$lng]; ?>"></a>
				<?php } ?>
				<img src="<?php echo $block['images']['img']; ?>" alt="<?php echo $block['images']['img_alt'][$lng]; ?>" class="img-responsive" />
				<figcaption>
					<h2 class="h4"><?php echo html_entity_decode($block['title'][$lng], ENT_QUOTES, 'UTF-8'); ?></h2>
					<p class="description"><?php echo html_entity_decode($block['description'][$lng], ENT_QUOTES, 'UTF-8'); ?></p>
				</figcaption>
			</figure>
		<?php break;

		case '5': ?>
			<figure class="effect-5 h-effect">
				<img src="<?php echo $block['images']['img']; ?>" alt="<?php echo $block['images']['img_alt'][$lng]; ?>" class="img-responsive" />
				<figcaption>
					<h2 class="h3"><?php echo html_entity_decode($block['title'][$lng], ENT_QUOTES, 'UTF-8'); ?></h2>
					<p><?php echo html_entity_decode($block['description'][$lng], ENT_QUOTES, 'UTF-8'); ?></p>
					<?php if ($block['images']['url'] != '') { ?>
					<a href="<?php echo $block['images']['url']; ?>"<?php echo $block['images']['win']; ?>><img src="image/spacer.gif" alt="<?php echo $block['images']['img_alt'][$lng]; ?>"></a>
					<?php } ?>
				</figcaption>
			</figure>
		<?php break;

		case '6': ?>
			<figure class="effect-6 h-effect">
				<img src="<?php echo $block['images']['img']; ?>" alt="<?php echo $block['images']['img_alt'][$lng]; ?>" class="img-responsive" />
				<figcaption>
					<h2 class="h3"><?php echo html_entity_decode($block['title'][$lng], ENT_QUOTES, 'UTF-8'); ?></h2>
					<p><?php echo html_entity_decode($block['description'][$lng], ENT_QUOTES, 'UTF-8'); ?></p>
					<?php if ($block['images']['url'] != '') { ?>
					<a href="<?php echo $block['images']['url']; ?>"<?php echo $block['images']['win']; ?>><img src="image/spacer.gif" alt="<?php echo $block['images']['img_alt'][$lng]; ?>"></a>
					<?php } ?>
				</figcaption>
			</figure>
		<?php break;
			
		default: ?>
			<?php if ($block['images']['url'] != '') { ?>
			<a class="bnr-img" href="<?php echo $block['images']['url']; ?>"<?php echo $block['images']['win']; ?>><img src="<?php echo $block['images']['img']; ?>" alt="<?php echo $block['images']['img_alt'][$lng]; ?>" class="img-responsive" /></a>
			<?php } else { ?>
			<span class="bnr-img"><img src="<?php echo $block['images']['img']; ?>" alt="<?php echo $block['images']['img_alt'][$lng]; ?>" class="img-responsive" /></span>
			<?php } ?>
		<?php break;
	} ?>

</div>