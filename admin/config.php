<?php
// HTTP
define('HTTP_SERVER', 'http://localhost/ravena/admin/');
define('HTTP_CATALOG', 'http://localhost/ravena/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/ravena/admin/');
define('HTTPS_CATALOG', 'http://localhost/ravena/');

// DIR
define('DIR_APPLICATION', '/var/www/html/ravena/admin/');
define('DIR_SYSTEM', '/var/www/html/ravena/system/');
define('DIR_LANGUAGE', '/var/www/html/ravena/admin/language/');
define('DIR_TEMPLATE', '/var/www/html/ravena/admin/view/template/');
define('DIR_CONFIG', '/var/www/html/ravena/system/config/');
define('DIR_IMAGE', '/var/www/html/ravena/image/');
define('DIR_CACHE', '/var/www/html/ravena/system/cache/');
define('DIR_DOWNLOAD', '/var/www/html/ravena/system/download/');
define('DIR_UPLOAD', '/var/www/html/ravena/system/upload/');
define('DIR_LOGS', '/var/www/html/ravena/system/logs/');
define('DIR_MODIFICATION', '/var/www/html/ravena/system/modification/');
define('DIR_CATALOG', '/var/www/html/ravena/catalog/');

// DB
define('DB_DRIVER', 'mpdo');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'root');
define('DB_DATABASE', 'ravena21_loja');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
