<?php 

	session_start(); 
?>

<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title>Login</title>
		<link type="text/css" rel="stylesheet" media="screen" href="css/bootstrap.min.css">
		<link type="text/css" rel="stylesheet" media="screen" href="css/admin.css">
		<script type="text/javascript" src="js/jquery.js"></script>

		<script>
			
			$(document).ready(function() {
				if("<?php echo $_SESSION['msg'];?>" != ""){
					$(".alert").html("<?php echo $_SESSION['msg'];?>");
				}		
			})		
		</script>
	</head>
	<body>
		
		<div class="navbar navbar-fixed-top">
			<div class="navbar-inner">
				<div class="container">
					<a data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</a>

				</div>
			</div><!-- /navbar-inner -->
		</div>
		<div class="container">
			<div class="hero-unit">
				<div id="login-top"> </div>
				<p class="alert alert-error">Voc&ecirc;  precisa estar logado para continuar.</p>
				<h4>Login</h4>
				<form class="form-horizontal" action="login.php" method="post">
					<div class="control-group">
						<label class="control-label">Login</label>
						<div class="controls">
							<input id="login" type="text" name="login" autofocus="autofocus" size="30" placeholder="Login" />
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Senha</label>
						<div class="controls">
							<input id="password" type="password" name="password" size="30" placeholder="Senha"/>
						</div>
					</div>
					<div class="control-group">
						<div class="controls">
							<input class="btn" type="submit" value="Entrar" name="commit">
						</div>
					</div>
				</form>
			</div>
		</div>
	</body>
</html>
